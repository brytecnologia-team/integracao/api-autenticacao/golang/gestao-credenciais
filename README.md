# Emissão e Gestão de Credenciais

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

O access token armazenado em memória válido e monitorado é disponibilizado na forma de serviço através do endereço: http://localhost:8080/jwt

Abaixo seguem os passos principais:
* Ao iniciar, a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token**.

### Tech

O exemplo utiliza as seguintes tecnologias e bibliotecas Java abaixo:
* [Golang Version] - 1.12.9
* [Package dgrijalva/jwt-go] - Package jwt is a Go implementation of JSON Web Tokens
* [Package gorilla/mux] - Package mux implements a request router and dispatcher.
* [Package robfig/cron] - Package cron implements a cron spec parser and job runner.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável | Descrição | Arquivo |
| ------ | ------ | ------ |
| <CRON_EXPRESSION> | Endereço do endpoint de obtenção de credenciais. | credential_config.go
| <OAUTH2_CLIENT_ID> | **client_id** de uma aplicação. | credential_config.go
| <OAUTH2_CLIENT_SECRET> | **client_secret** de uma aplicação. | credential_config.go

### Observações

Os atributos de data e hora codificados nos tokens JWT (access token e refresh token), seguem o fuso horário UTC-0.
Lembrando que o Brasil possui 4 fusos horários:
* UTC-2 - Atol das Rocas e Fernando de Noronha.
* UTC-3 **(oficial)** - Estados não citados nos demais itens.
* UTC-4 - Amazonas, Mato Grosso, Mato Grosso do Sul, Rondônia e Roraima.
* UTC-5 - Acre e alguns municípios do Amazonas.


### Uso

Para execução do projeto:

    go mod download
    go run main.go


[Golang Version]: https://golang.org/dl/
[dgrijalva/jwt-go]: https://godoc.org/github.com/dgrijalva/jwt-go
[gorilla/mux]: https://godoc.org/github.com/gorilla/mux
[robfig/cron]: https://godoc.org/github.com/robfig/cron


