package config

const(
	//cron interval time in seconds
	CronExpression = 30 //<CRON_EXPRESSION>
)

const(
	Oauth2ProviderAddress = "https://cloud.bry.com.br/token-service/jwt"
	Oauth2CredentialsClientId = "<OAUTH2_CLIENT_ID>"
	Oauth2CredentialsClientSecret= "<OAUTH2_CLIENT_SECRET"
)
