package controller

import (
	"encoding/json"
	"gestao-credenciais/service"
	"log"
	"net/http"
)

var GenerateAccessToken = func(w http.ResponseWriter, r * http.Request) {
	log.Println("Request: Recovering from service the current in-memory credential reference.")
	credentialResponse, err := service.GenerateAccessToken()
	if err != nil {
		log.Println("Request: " + err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("Request: Sending credential response")
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(credentialResponse)
}
