package main

import (
	"gestao-credenciais/controller"
	"gestao-credenciais/service"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main(){
	//responsible for initialize the credential that will be stored in memory.
	_,err := service.GenerateAccessToken()
	if err != nil {
		log.Fatal(err)
	}

	//periodically validate the in-memory BRy Cloud platform credential.
	service.StartTask(service.CheckCredentialExpiration)

	//starting http server on port 8080
	router := mux.NewRouter()
	router.HandleFunc("/jwt", controller.GenerateAccessToken)
	_ = http.ListenAndServe(":8080", router)
}
