package mapper

import (
	"encoding/json"
	"gestao-credenciais/model"
	"io/ioutil"
	"net/http"
)

type JsonMapper struct {}


func (jsonMapper JsonMapper) DecodeToString(response *http.Response) (string, error){
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func (jsonMapper JsonMapper) DecodeToCredentialResponse(response *http.Response) (*model.CredentialResponse, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var credentialResponse model.CredentialResponse
	err = json.Unmarshal(bytes, &credentialResponse)
	return &credentialResponse, err
}

