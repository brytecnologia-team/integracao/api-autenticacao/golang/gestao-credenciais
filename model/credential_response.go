package model

type CredentialResponse struct {
	AccessToken			string `json:"access_token"`
	RefreshToken 		string `json:"refresh_token"`
	ExpiresIn 			int64 `json:"expires_in"`
	RefreshExpiresIn 	int64 `json:"refresh_expires_in"`
	TokenType 			string `json:"token_type"`
}
