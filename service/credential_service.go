package service

import (
	"errors"
	"gestao-credenciais/config"
	"gestao-credenciais/mapper"
	"gestao-credenciais/model"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var credentialResponse *model.CredentialResponse

func GenerateAccessToken() (*model.CredentialResponse, error) {
	var err error
	if credentialResponse == nil {
		log.Println("Service: Credential is empty, requesting a new access token")
		credentialResponse, err = postAccessTokenRequest()
		if err != nil {
			log.Println(err)
		}
		return credentialResponse, err
	}
	log.Println("Service: Checking in-memory Credential")
	claims, err := GetClaims(credentialResponse.AccessToken)
	if err != nil {
		return credentialResponse, err
	}
	expClaim, ok := claims["exp"]
	if !ok || expClaim == nil {
		return credentialResponse, errors.New("claim exp doesnt exist")
	}
	exp := time.Unix(int64(expClaim.(float64)),0).UTC()
	now := time.Now().UTC()

	if exp.Before(now) {
		log.Println("Service: Credential is expired, requesting a new access token")
		credentialResponse, err = postAccessTokenRequest()
		return credentialResponse, err
	}
	return credentialResponse, nil
}


func RenewAccessToken(refreshToken string) (*model.CredentialResponse, error) {
	var err error
	claims, err := GetClaims(refreshToken)
	if err != nil {
		return nil, err
	}
	expClaim, ok := claims["exp"]
	if !ok || expClaim == nil {
		return nil, errors.New("claim exp not found")
	}
	exp := time.Unix(int64(expClaim.(float64)),0).UTC()
	now := time.Now().UTC()
	if exp.Before(now) {
		return nil, errors.New("refresh token expired")
	}
	log.Println("Service: Requesting a new access token using refresh token")
	credentialResponse, err = postRefreshTokenRequest(refreshToken)
	return credentialResponse, err
}

func postAccessTokenRequest() (*model.CredentialResponse, error) {
	data := url.Values{}
	data.Set("grant_type", "client_credentials")
	data.Set("client_id", config.Oauth2CredentialsClientId)
	data.Set("client_secret", config.Oauth2CredentialsClientSecret)

	req, err := http.NewRequest(http.MethodPost, config.Oauth2ProviderAddress, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	return mapper.JsonMapper{}.DecodeToCredentialResponse(resp)
}

func postRefreshTokenRequest(refreshToken string) (*model.CredentialResponse, error) {
	data := url.Values{}
	data.Set("grant_type","refresh_token")
	data.Set("refresh_token", refreshToken)

	req, err := http.NewRequest(http.MethodPost, config.Oauth2ProviderAddress, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	return mapper.JsonMapper{}.DecodeToCredentialResponse(resp)
}
