package service

import (
	"errors"
	"fmt"
	"gestao-credenciais/config"
	"github.com/robfig/cron"
	"log"
	"time"
)

func StartTask(cmd func()) {
	cronExpression :=fmt.Sprintf("@every %ds",config.CronExpression)
	c := cron.New()
	c.AddFunc(cronExpression, cmd)
	c.Start()
}

func CheckCredentialExpiration(){
	log.Println("Recovering from service the current in-memory credential reference.")
	credentialResponse, err := GenerateAccessToken()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Parsing the access token (JWT) string.")
	claims, err := GetClaims(credentialResponse.AccessToken)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("From the claims list, obtain the expiration time and converts to the comparison representation.")
	expClaim, ok := claims["exp"]
	if !ok || expClaim == nil {
		log.Println(errors.New("claim exp not found"))
		return
	}
	exp := time.Unix(int64(expClaim.(float64)),0).UTC()
	now := time.Now().UTC()

	if exp.Before(now) {
		log.Println("Access token expired, starting access token renew")
		_, err = RenewAccessToken(credentialResponse.RefreshToken)
		if err != nil {
			log.Println(err)
		}
		return
	} else {
		log.Printf("Access token still valid remaining_time=%s exp=%s now=%s", exp.Sub(now).String(), exp.String(), now.String())
	}
}

