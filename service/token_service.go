package service

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
)

type Claims jwt.MapClaims

func GetClaims(token string) (Claims, error) {
	jwtToken, _, err := new(jwt.Parser).ParseUnverified(token, jwt.MapClaims{})
	if err != nil {
		return nil, err
	}
	if claims, ok := jwtToken.Claims.(jwt.MapClaims); ok {
		jwtToken := Claims(claims)
		return jwtToken, nil
	}
	return nil, errors.New("cannot get claims from token")
}

